//
//  LoginView.swift
//  StateAndDataFlow
//
//  Created by Alexey Efimov on 26.04.2023.
//

import SwiftUI

struct LoginView: View {
//    @State private var name = ""
    @EnvironmentObject private var user: UserSettings
    
    var body: some View {
        VStack {
            HStack{
                TextField("Enter your name", text: user.$name)
                    .multilineTextAlignment(.center)
                    .frame(width: 250)
                Text("\(user.name.count)")
                    .foregroundColor(user.name.count < 3 ? .gray : .green )
            }
            Button(action: user.login) {
                HStack {
                    Image(systemName: "checkmark.circle")
                    Text("Ok")
                }
            }
            .disabled(user.name.count < 3)
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
